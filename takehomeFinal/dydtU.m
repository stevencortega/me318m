function answer = dydtU(t, y)
    R = 0.1;
    C = 1.2;
    answer = (-y)/(R*C);
end