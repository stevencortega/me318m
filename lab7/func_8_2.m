% Problem 8.2 Function
% Lab 7

function result = func_8_2(angles)
    % define the constants/knowns
    d1 = 1;             % [m]
    d2 = 1;             % [m]
    rdes = 1;           % [m]
    hdes = 1.1;         % [m]

    % separate alpha and beta
    alpha = angles(1);
    beta = angles(2);

    % compute f1 and f2
    r = (d1 .* cos(alpha)) + (d2 .* cos(alpha + beta)) - rdes;
    h = (d1 .* sin(alpha)) + (d2 .* sin(alpha + beta)) - hdes;

    result = [r ; h];
end
