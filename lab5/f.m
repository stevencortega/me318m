% Function 5.1

function [output] = f(x)
    output = (3/500) .* (x.^3 - (18.535 .* x.^2) + (25.697 .* x) + 28.099);
end