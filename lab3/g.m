function value = g(x)
    value = -1.25 .* x .* exp(sin(x)) .* exp(-3 .* x) - 71.4;
end