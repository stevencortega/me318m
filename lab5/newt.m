% Newton's Method Algorithm
%
% input:
%   x = initial guess
% output:
%   root = root of f(x) if found

function root = newt(x)
    count = 0;  % begin tracking iterations
    while 1
        % compute the new root
        x = x - (f(x)/fprime(x));

        % exit conditions
        if (abs(f(x)) < 1e-3)
            root = x;
            return
        elseif count > 100
            disp('Root not found within 100 iterations')
            return
        end
        count = count + 1;
    end
end