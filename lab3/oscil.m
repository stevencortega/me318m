clear; clc;

%% Graph 1
a = 0.5 ;
b = -0.5 ;
t = 0:.01:15;

% create values for the graph
for i=1:length(t)
    % The code for the equation for x goes here
    x(i) = sin(a*t(i)) * exp(b*t(i));
    % The code for the equation for x2 goes here
    x2(i) = sin(a*t(i)) * cos(a*t(i));
    % The code for the equation for v goes here
    v(i) = (a*cos(a*t(i)) * exp(b*t(i))) + (b*sin(a*t(i)) * exp(b*t(i)));
end

% create the visual graph
plot(t,x)
% user readability
title('Position vs. Time');
xlabel('Time (s)');
ylabel('Position (m)');

%% Graph 2
% create a second graph
figure(2);
plot(t,v);
% user readability
title('Velocity vs. Time');
xlabel('Time (s)');
ylabel('Velocity (m/s)');

%% Bar 1
%create a bar graph
temp = [24 30 60 68 79 85 87 89];
time = 0:1:7;
figure(3), bar(time,temp);
title('Engine Temperature graph');
xlabel('Time (secs)')
ylabel('Temperature (degrees)');

%% Plot3 1
theta = 0:pi/50:10*pi;
plot3(sin(theta),cos(theta),theta)
xlabel('sin(theta)')
ylabel('cos(theta)')
zlabel('theta')
grid on
axis square

%% 3D Plot
x1 = 0:4; x2 = 0:5;
% x1g = [
%     0 1 2 3 4;
%     0 1 2 3 4;
%     0 1 2 3 4;
%     0 1 2 3 4;
%     0 1 2 3 4;
%     0 1 2 3 4];
% 
% x2g = [
%     0 0 0 0 0;
%     1 1 1 1 1;
%     2 2 2 2 2;
%     3 3 3 3 3;
%     4 4 4 4 4;
%     5 5 5 5 5];

[x1g, x2g] = meshgrid(x1, x2);
y=x1g.^2 + x2g.^2;

surf(x1g, x2g, y)
xlabel('X')
ylabel('Y')
zlabel('Z')
grid on

% The angles with the greatest range? I'm not sure how to know what angle
% it is but the colors maxout in the x y and z at one edge so I assume that
% means there the range is the greatest in all directions. Probably
% parrallel to any plane. 90 degrees

%The robot also doesnt look like it can reach into negative values since it
%curves off towards 0. Probably the range is a sphere and the bottom
%touches the origin.

% with no line style the lines disappear and you can only see the white
% plane/surface. It would be better looking if it was using surf.

%% Subplot

t = -1.07:0.001:1.07;
figure(1),
title('Square Arrangement');
subplot(2,2,1), plot(t, sin(t))
title('A sine function');
ylabel('Amplitude');
xlabel('Angle in radians');
subplot(2,2,2), plot(t, cos(t))
title('A cosine function');
ylabel('Amplitude');
xlabel('Angle in radians');
subplot(2,2,3), plot(t, tan(t))
title('A tangent function');
ylabel('Amplitude');
xlabel('Angle in radians');
subplot(2,2,4), plot(t, sec(t))
title('A Secant function');
ylabel('Amplitude');
xlabel('Angle in radians')

%% Saveas
t = 0:0.001:10;
a = 2*t.^2 + t -2;
b = sin(t);
c = cos(t);

h1 = figure(1),
plot(t,a);
h2 = figure(2),
plot(t,b);
h3 = figure(3),
plot(t,c);
saveas(h1,'fancy.pdf');
saveas(h2,'sin.bmp');
saveas(h3,'cos.jpg');