function [out] = myfun(time)
    out = exp(-time) * cos((10 * time) - 1);
end