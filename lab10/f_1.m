% Calculate the derivative (Eq. 11.1)

function answer = f_1(v, rho, p, g)
    answer = (-rho*(v^p)) + g;
end