% is_palindrome
% checks the string to see if it is a palindrome or not and displays a
% message with the result
%
% Input: string s
% Output: None

function is_palindrome(s)
    %sRev = reverse(s);
    
    % ------- long way --------
    
    revStr = '';
    for i = 0:1:(length(s)-1)
        currentLetter = s(length(s)-i);
        revStr = append(revStr, currentLetter);
    end
    % --------------------------
    
    
    for i = 1 : 1 : ceil(length(s)/2)
        if strcmpi(s(i), revStr(i)) == false
            disp(s + " is not a palindrome")
            return
        end
    end
    disp(s + " is a palindrome")

    
    
end
