%%% Lab 1 Assignment
% Date: 2/4/2022
% Name: Steven Ortega
% Section: 18265
clear all; clc;


%% Problem 1.1
% 2more is invalid because it begins with a number
% i-p-freely is invalid because it has '-'
% lucky# is invalid because of the '#'


%% Problem 1.2
%>>grav_constant = 6.67e-11 ;
% 
%>>grav_constant
%  grav_constant =
% 
%  6.6700e-11


%% Problem 1.3
% >> x = 4e-5 + (3e-3 * i);
% >>x
% x =
% 
%    0.0000 + 0.0030i

% this was unexpected but since the real part has more than
% 4 decimals it won't show. We can fix this with format long or format
% shortG


%% Problem 1.4
% 3.141592653589793



%% Problem 1.5
% As j increases the error (diff) increases. The idea of accuracy and
% error is applicable to important calculations. Since the irrational value
% of pi has many more digits than matlabs version or the Pi5 version we can
% never truly get the real pi value and error will always exist. By using
% more digits we can make the error smaller until we can set a tolerance
% that will provide accurate results.


%% Problem 1.6
% >>t = y^x + w * w + v
% 
% t =
% 
%      4.993600000000000e+02




%% Problem 1.7
% the last two exp
% y-(57^1.1)/10+a
% y-57^1.1/10+a


%% Problem 1.8
Var1 = 1.372;
Var2 = 3.102;
Var3 = 2.763;
Var4 = 2.718;

% steps:
step1 = (Var1+Var2)*(3.25^Var3)-sin(pi/1.085);
step2 = Var2 - (Var1*exp(Var3/0.566))-asin(sqrt(3)/2);
step3 = Var1 - Var2^(Var3-Var4);
step4 = step1*step2 + step3;
fprintf('The answer to step 4 is: %f \n', step4);


%% Problem 1.9
%x = input('First number to sum?:');
x = 1;
%y = input('Second number to sum?:');
y = 2;
% print result to 2 decimals to keep it clean
fprintf('You entered %.2f and %.2f which sum to %.2f\n', x, y, (x+y));


%% Problem 1.10
%favfood = input('Please tell me favorite food?','s');
favfood = 'sushi';
% yes the typo above is intentional (lab handout has typo)
fprintf('You just told me that %s is your favorite food.\n', favfood);
%showing with the value of favfoov
fprintf('The value of favfood is: %s\n', favfood);


%% Problem 1.11
%num1 = input('Enter num1:');
num1 = 2;
%num2 = input('Enter num2:');
num2 = 3;
save('problem_1-11.mat', 'num1', 'num2');

load('problem_1-11.mat')
num3 = num1 + num2;
save('problem_1-11.mat', 'num3');


%% Additional Problem 1
%usr_age = input('How old are you?');
usr_age = 12;
fprintf('Wow, you are %i years old. How cool :)\n', usr_age)

%results:
% Wow, you are 10 years old. How cool :)
% Wow, you are 86 years old. How cool :)
% Wow, you are -10 years old. How cool :)

%% Additional Problem 2
%usr_age2 = input('How old are you?');
usr_age2 = 21;
if (usr_age2 >= 50)
    fprintf('Are you retired yet?\n')
elseif (usr_age2 >= 30)
    fprintf("Oh that's wonderful! Do you have kids?\n")
elseif (usr_age2 >= 20)
    fprintf("That's the best age!\n")
elseif (usr_age2 >= 10)
    fprintf("Wow, you're so young!\n")
elseif (usr_age2 > 0)
    fprintf("Oh that's so cute!\n")
end

% results:
% 76 -> Are you retired yet?
% 21 -> That's the best age!
% -5 -> [no output, program doesnt print and ends]
% 1 million -> Are you retired yet? (no upper limit on age)