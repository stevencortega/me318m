%%% Lab 7 Assignment
% Date: 4/3/2022
% Name: Steven Ortega
% Section: 18265
clear all; clc;


%% Problem 8.1

% define the constants/knowns
d1 = 1;             % [m]
d2 = 1;             % [m]
alpha = 0 : pi/100 : pi/2;      % [radians]
beta = 0 : pi/100 : pi;         % [radians]

% create the plot intervals and calculate range
[x, y] = meshgrid(alpha, beta);

r = (d1 .* cos(x)) + (d2 .* cos(x + y));
h = (d1 .* sin(x)) + (d2 .* sin(x + y));

figure(1)
% Plot the range
subplot(1,2,1), mesh(x,y,r)
    title('Range of robot arm')    
    xlabel('Alpha [rads]')
    ylabel('Beta [rads]')
    zlabel('Range [m]')
    
% Plot the height
subplot(1,2,2), mesh(x,y,h)
    title('Height of robot arm')
    xlabel('Alpha [rads]')
    ylabel('Beta [rads]')
    zlabel('Height [m]')

% Discussion
% 1) The angles with the greatest heights are when beta is 0 and alpha is
%       at maximum (aka pi/2, straight up)
% 2) The Height can never go negative because alpha is always > 0 and so is
%       beta. Since neither angle is ever negative the arm never goes below
%       base height. This makes sense because alpha is only 0-90 degrees
%       and beta can only bend until it is back at the origin.

fprintf('Problem 8.1 RAN\n\n')

%% Problem 8.2

disp('Look at func_8_2.m and jacob_8_2.m')

fprintf('Problem 8.2 RAN\n\n')

%% Problem 8.3
    countlimit = 50;            % get the count limit
    xold = [0; pi/2];        % arbitrary initial guess
    tol = .1;                 % arbitrary tolerance
    count = 0;                  % begin tracking iterations

    while 1
        % compute the new root
        xnew = xold - (jacob_8_2(xold) \ func_8_2(xold));

        % exit conditions
        if (abs(func_8_2(xold) - func_8_2(xnew)) < tol)
            disp('The angles are [rads]:')
            disp(xnew)
            break;

        elseif count > countlimit
            disp('Root not found within 100 iterations')
            break;
        end

        % update variables and continue loop
        count = count + 1;
        xold = xnew;
    end

fprintf('Problem 8.3 RAN\n\n')

%% Problem 8.4

    countlimit = 100;            % get the count limit
    xold = [0; pi/4];        % arbitrary initial guess
    tol = .01;                 % arbitrary tolerance
    count = 0;                  % begin tracking iterations

    while 1
        % compute the new root
        xnew = xold - (jacob_8_2(xold) \ func_8_2(xold));

        % exit conditions
        if (abs(func_8_2(xold) - func_8_2(xnew)) < tol)
            disp('The angles are [rads]:')
            disp(xnew)
            disp('The function values:')
            disp(func_8_2(xnew))
            fprintf('Number of iterations: %i\n', count)
            
            break;

        elseif count > countlimit
            disp('Root not found within 100 iterations')
            break;
        end

        % update variables and continue loop
        count = count + 1;
        xold = xnew;
    end

% The angles are alpha= 0.1001 rads(5.74 deg), beta= 1.4677(84.09 deg)
% It makes sense since alpha is within 0 and pi/2 and beta is within 0 and
% pi.

% I'm not exactly sure but it has to do with ill conditioned jacobeans and
% the det of the jacobeans. I noticed with some values I would get errors
% regarding the conditioning.
fprintf('Problem 8.4 RAN\n\n')