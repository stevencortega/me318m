%%% Final Exam
% Date: 5/6/2022
% Name: Steven Ortega
% Section: 18265
clear all; clc;
%% Problem 1

% Part A
% Load the data
data = load("current_data.mat");
current = data.current;
time = data.t;

C = 1.2;

% Use Simpson's Rule to find voltage
n = length(time);
m = n-1;

bma = time(end)-time(1);    % bounds

sum4 = 0;	% Summation with weight of 4
sum2 = 0;	% Summation with weight of 2

% Compute the sum4 terms
for i = 2 : 2 : n-1
	sum4 = sum4 + current(i); 
end

% Compute the sum2 terms
for i = 3 : 2 : n-2
	sum2 = sum2 + current(i);
end

% Simpsons rule
Y = ((bma) * (current(1) + 4*sum4 + 2*sum2 + current(end)))/(3*m);
dydt = Y/C;
fprintf('The voltage at t = 1 is: %.3f\n', dydt)

% ---------------------------------
% Quadratic Fit
% ---------------------------------

% Compute Sum terms
sumx4 = 0; sumx3 = 0; sumx2 = 0; sumx= 0; sumxy = 0; sumx2y = 0; sumy = 0;
for i = 1 : n
    sumx4 = sumx4 + time(i)^4;
    sumx3 = sumx3 + time(i)^3;
    sumx2 = sumx2 + time(i)^2;
    sumx = sumx + time(i);
    sumy = sumy + current(i);
    sumxy = sumxy + time(i)*current(i);
    sumx2y = sumx2y + current(i)*time(i)^2;
end
% Compute the A Matrix
A = [sumx4, sumx3, sumx2;
     sumx3, sumx2, sumx;
     sumx2, sumx, n];

% Compute the B Matrix
B = [sumx2y;
     sumxy;
     sumy];

% Solve the System to get the coefficients
coeff = A\B;
coeff = coeff/C;

fprintf('The quadratic fit is: %.2fx^2 + %.2fx + %.2f\n', coeff)

%--------------------
% Part C

% compute the quadratic best fit
quad = coeff(1)*(time.^2) + coeff(2)*time + coeff(3);

figure(1)
plot(time, current/C,'o-', time, quad, 'r.-')
title("Original Data vs. Best Fit Line")
xlabel("Time [s]")
ylabel("Derivative of Voltage")
legend("Collected Data", "Quadratic Best Fit")

%% Problem 2
% Part A

% Get the variable values
t0 = input('Enter t0: ');
tf = input('Enter tf: ');
y0 = input('Enter y0: ');
h = input('Enter Step Size h: ');

R = 0.1;
C = 1.2;
t = t0:h:tf;
yU(1) = y0;
yF(1) = y0;

% Calculate both unforced and forced at the same time
for i = 1 : length(t) - 1
    yU(i+1) = yU(i) + dydtU(t(i), yU(i))*h;
    yF(i+1) = yF(i) + dydtF(t(i), yF(i))*h;
end

disp("The Unforced System Values:")
disp(yU)
fprintf('\n\n')
disp("The Forced System Values:")
disp(yF)

%---------------------------------
% Part B

% Prompt the User
h2 = 0;
while h2 ~= 0.01
    h2 = input('Enter Step Size h2 of 0.01: ');
end

t2 = t0:h2:tf;
yU2(1) = y0;
yF2(1) = y0;

% Calculate both unforced and forced for new h at the same time
for i = 1 : length(t2) - 1
    yU2(i+1) = yU2(i) + dydtU(t2(i), yU2(i))*h2;
    yF2(i+1) = yF2(i) + dydtF(t2(i), yF2(i))*h2;
end

% Plot the Functions
figure(2)
plot(t, yU,'.-', t, yF,'r.-')
hold on 
plot(t2, yU2, 'g.-', t2, yF2, 'b.-')
hold off
title("Voltage vs. Time")
xlabel("Time [s]")
ylabel("Voltage")
legend("Unforced, h = 0.05", "Forced, h = 0.05", "Unforced, h = 0.01", "Forced, h = 0.01")


%% Problem 3
% Part C
h = 0.7;
t = 0: h :10;
x1(1) = pi/20;
x2(1) = 0;

for i = 1:(length(t)-1)
    x1_prime = x2(i);
    x1p = x1(i) + h*x1_prime;

    x2_prime = dxdt(t(i), x1(i), x2(i));
    x2p = x2(i) + h*x2_prime;

    x1n_prime = x2p;
    x2n_prime = dxdt(t(i),x1p, x2p);
    
    x1(i+1) = x1(i) + (h/2)*(x1_prime+x1n_prime);
    x2(i+1) = x2(i) + (h/2)*(x2_prime+x2n_prime);
end
figure(3)
plot(t, x1, 'b')

figure(4)
plot(t,x2, 'b')


h = 0.65;
t = 0: h :10;
x1(1) = pi/20;
x2(1) = 0;

for i = 1:(length(t)-1)
    x1_prime = x2(i);
    x1p = x1(i) + h*x1_prime;

    x2_prime = dxdt(t(i), x1(i), x2(i));
    x2p = x2(i) + h*x2_prime;

    x1n_prime = x2p;
    x2n_prime = dxdt(t(i),x1p, x2p);
    
    x1(i+1) = x1(i) + (h/2)*(x1_prime+x1n_prime);
    x2(i+1) = x2(i) + (h/2)*(x2_prime+x2n_prime);
end
figure(3)
hold on;
plot(t, x1, 'g')
hold off;
figure(4)
hold on;
plot(t,x2, 'g')
hold off;


h = 0.1;
t = 0: h :10;
x1(1) = pi/20;
x2(1) = 0;

for i = 1:(length(t)-1)
    x1_prime = x2(i);
    x1p = x1(i) + h*x1_prime;

    x2_prime = dxdt(t(i), x1(i), x2(i));
    x2p = x2(i) + h*x2_prime;

    x1n_prime = x2p;
    x2n_prime = dxdt(t(i),x1p, x2p);
    
    x1(i+1) = x1(i) + (h/2)*(x1_prime+x1n_prime);
    x2(i+1) = x2(i) + (h/2)*(x2_prime+x2n_prime);
end
figure(3)
hold on;
plot(t, x1, 'r')
legend('h = 0.7', 'h = 0.65', 'h = 0.15')
title('Time vs. Angle')
xlabel('Time [s]')
ylabel('Angle [rads]')
hold off;
figure(4)
hold on;
plot(t,x2, 'r')
legend('h = 0.7', 'h = 0.65', 'h = 0.15')
title('Time vs. Anglular Velocity')
xlabel('Time [s]')
ylabel('Angular Velocity [rads/s]')
hold off;
