function answer = dxdt(t, x1, x2)
    M = 75;
    m = 5;
    l = 2;
    g = 9.81;
    answer = (((3*t) + 100) - ((M+m)*((2*(l^2)*x2) + (g*x1^3)))/M*l);
end