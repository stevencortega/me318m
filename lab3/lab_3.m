%%% Lab 3 Assignment
% Date: 2/21/2022
% Name: Steven Ortega
% Section: 18265
clear all; clc;
% =========================
% NOTE: To run a section of code, click inside the section you want to run
%       (e.g. Problem 3.1), then select "Run Section" from the menu above

%% Problem 3.1
% >> inputvector = [0 3 3.3];
% >> f(inputvector)
% 
% ans =
% 
%    -0.1086   -0.0529   -0.0725

disp('Problem 3.1 RAN')
%% Problem 3.2
% >> fscalar(0.67)
% 
% ans =
% 
%    -0.0319
% 
% >> vectorinput= [1 -2 2];
% >> fscalar(vectorinput)
% Error using  ^  (line 52)
% Incorrect dimensions for raising a matrix to a power.
% Check that the matrix is square and the power is a
% scalar. To perform elementwise matrix powers, use '.^'.
% 
% Error in fscalar (line 2)
%     value = (3/500)*(x^3 - 10.535*x^2 + 25.697*x - 18.099) ;
%  
% Answer/Explanation:
% Since I used ^ instead of .^ I get an error inputing a vector into my
% scalar function fscalar.

disp('Problem 3.2 RAN')

%% Problem 3.3

% ----- Both functions are in separate files, will upload to GitLab -----

% function value = g(x)
%     value = -1.25 .* x .* exp(sin(x)) .* exp(-3 .* x) - 71.4;
% end

% function value = h(x)
%     if x < 12
%         value = 0;
%     else
%         value = (12*x^2) - 12;
%     end
% end

disp('Problem 3.3 RAN')

%% Problem 3.4
% ----------------------------------------------------
% Part A)

for n = 1:2
    zVal = input('What value do you want to compute?');
    nVal = input('How many terms to compute in the calculation?');
    
    % Compute through summation
    seriesAns = 0;
    for k = 0 : (nVal - 1)
       seriesAns = seriesAns + ((-1)^k * zVal^(2*k + 1))/factorial(2*k + 1);
    end
    
    % Compute through products
    productAns = zVal;
    for k = 1 : nVal
        productAns = productAns * ( 1 - ( zVal/(k*pi()) )^2 );
    end
    
    fprintf('The value %.5f computed is:\n', zVal)
    fprintf(' Summation: %f\n Product: %f\n', seriesAns, productAns)

end

% ----------------------------------------------------
% Part B)
fprintf('------Part B: Comparing sine functions and values------\n\n')
fprintf('The computed values are: \nSummation: %.5f\n', seriesAns)
fprintf('Products: %.5f\n VS. Matlab sine: %.5f\n', productAns, sin(zVal))

% ----------------------------------------------------
% Part C)
zVal = input('Value?');
usr_in = 0; % 
while usr_in
    nVal = input('How many terms?');
    
    % Compute through summation
    seriesAns = 0;
    for k = 0 : (nVal - 1)
       seriesAns = seriesAns + ((-1)^k * zVal^(2*k + 1))/factorial(2*k + 1);
    end
    
    % Compute through products
    productAns = zVal;
    for k = 1 : nVal
        productAns = productAns * ( 1 - ( zVal/(k*pi()) )^2 );
    end
    
    fprintf('The value %.5f computed is:\n', zVal)
    fprintf(' Summation: %.5f\n Product: %.5f\n Actual: %.5f\n', seriesAns, productAns, sin(zVal))
    
    usr_in = input('Is this accurate?');
end

% This code for pi/6 -> resulting in 0.5 had the following number of
% terms: Summation - 3 terms, Products - 2778 terms.
disp('Problem 3.4 RAN')


%% Problem 3.5

% import data
load('data.mat');

% assign plots to figure #1
figure(1)

% Create plot 1
subplot(2, 2, 1), plot(time, xdisp)
    title('Spring Mass Damper');
    xlabel('Time (s)');
    ylabel('Displacement (m)');
    grid on;

% Create plot 2
subplot(2, 2, 2), bar(1:12, measles)    % use a 12 month interval
    title('Measle Cases / Month');
    xlabel('Month');
    ylabel('# of Cases');
    grid on;

% Create plot 3
x1 = -3 : 0.1 : 3;              % create x-interval
y1 = -3 : 0.1 : 3;              % create y-interval
[x2, y2] = meshgrid(x1, y1);    % create the grid points
z = x2.^2 + y2.^2;              % assign values of z
subplot(2, 2, 3), mesh(x2, y2, z)
    title('f(x,y)');
    xlabel('x-axis');
    ylabel('y-axis');
    zlabel('z-axis');
    colorbar;                   % add a colorbar to increase readability
    
% Create plot 4
subplot(2, 2, 4), semilogx(frequency, magnitude, 'o')
    title('Frequency V. Magnitude');
    axis([0 1e6 -15 5]);
    xlabel('Frequency (dB)');
    ylabel('Magnitude (Hz)');
    grid on;


% create handles and save
h1 = figure(1);
saveas(h1, 'prob_5.jpg');

disp('Problem 3.5 RAN')

%% Problem 3.6

% ----------------------------------------------------
% Part A)

% get the values for the six variables
X1 = input('X1 Value:');
X2 = input('X2 Value:');
X3 = input('X3 Value:');
Y1 = input('Y1 Value:');
Y2 = input('Y2 Value:');
Y3 = input('Y3 Value:');

% save the variables in Problem6.mat
save('Problem6.mat', "X1", "X2", "X3", "Y1", "Y2", "Y3");

% ----------------------------------------------------
% Part B)

% load the variables from Problem6.mat
load("Problem6.mat");

% separate x and y values
xValues = [X1 X2 X3];
yValues = [Y1 Y2 Y3];

% find the mean of all values
allValues = [xValues yValues];
meanVal = mean(allValues);

% find distance betweent the points
diff = yValues - xValues;
xyDist = sqrt(diff(1)^2 + diff(2)^2 + diff(3)^2);

% ----------------------------------------------------
% Part C)

fprintf('The mean of the numbers is %.3f.\n', meanVal)
fprintf('The distance between the points X and Y is %.3f\n', xyDist)

disp('Problem 3.6 RAN')

