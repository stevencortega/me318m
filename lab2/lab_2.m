%%% Lab 2 Assignment
% Date: 2/14/2022
% Name: Steven Ortega
% Section: 18265
clc;

%% Variables made thorugh handout
A = [1 2 3 4];
B = [6 7 8 9]';
C = [1 2 3 4 5]';
K = [1 2 3; 0 4 6; 9 7 2];
L = [1 5; 3 4; 8 5];
%% Problem 2.1
x = 5; % will eval to false and disp nothing
if (x == 12) || (x == -3)
    disp('True')
end

disp('Problem 2.1 RAN')
%% Problem 2.2
usr_age = input('How old are you? ');
if (usr_age < 0)
    disp('I see you know the secrets of time')
elseif (usr_age > 0) && (usr_age<6)
    disp("Oh that's so cute!")
elseif (usr_age >= 6) && (usr_age < 23)
    disp("What school do you go to?")
elseif (usr_age >= 23) && (usr_age < 68)
    disp("Do you work?")
elseif (usr_age >= 68) && (usr_age < 1000000)
    disp("Are you retired?")
else
    disp('Are you a dinosaur?')
end

disp('Problem 2.2 RAN')
%% Problem 2.3
sum = 0;
for i = 0:2:200
    sum = sum + i;
end
fprintf('%i\n', sum)

disp('Problem 2.3 RAN')
%% Problem 2.4
response = 1;
counter = 0;
while(response ~= 0)
    fprintf('Hello Mr. Anderson!\nTimes repeated: %i\n', counter)
    response = input('Enter a number ~= 0 to repeat, Enter 0 to stop: ');
    if response ~= 0
        counter = counter + 1;
    end
end

disp('Problem 2.4 RAN')
%% Problem 2.5
disp("command: colvect = [ 5:7:33 ]';")
colvect = [ 5:7:33 ]'

disp('Problem 2.5 RAN')
%% Problem 2.6
M = [ 0 2 3 5 ;...
      7 3 8 4]

N = [ 1 1 3 ;  ...
      4 5 6 ;  ...
      9 4 8]

disp('Problem 2.6 RAN')
%% Problem 2.7
D = [-5.0 1.0 -3.0 3.0];
disp(A+D)
disp(A-D)
disp((2*D)-B')
disp(D-57)
disp(A/4)
disp(3*D)

disp('Problem 2.7 RAN')
%% Problem 2.8
disp('Command: D(4)')
disp(D(4))

disp('Problem 2.8 RAN')
%% Problem 2.9
% alt way: disp(find(N == 6))
disp('Command: N(2, 3) or find(N == 6)')
disp(N(2,3))

disp('Problem 2.9 RAN')

%% Problem 2.10
partOfM = M(:, 1:3)

disp('Problem 2.10 RAN')
%% Problem 2.11
% The following won't work because the indices must be a positive integer.
% Since the increment is .5, the second indice is 1.5 which is invalid.

disp('Problem 2.11 RAN')
%% Extra
x=1:0.5:6;
for i=1:length(x)
    y(i) = x( i ) + 1;
end

%% Problem 2.12
myValue = 9;
if myValue == 9
    n = myValue;
    for x = 1:n
        y(x) = exp(x);
    end
end
disp('Size:')
disp(size(y))
disp('Contents:')
disp(y)

%y should be have 9 elements, where each element is e^x where x is 1-9

disp('Problem 2.12 RAN')

%% Problem 2.13
disp('size(A)')
disp(size(A)) % 1 4
disp('size(M)')
disp(size(M)) % 3 2

disp('Problem 2.13 RAN')
