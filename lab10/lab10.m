%%% Lab 10 Assignment
% Date: 4/29/2022
% Name: Steven Ortega
% Section: 18265
clear all; clc;

%% Problem 11.1

t = 0 : .1 : 5;
rho = 1.7;
g = 32.2;
p = 1;

C = -g/rho;

for i = 1:length(t)
    v(i) = C*exp(-rho*t(i)) + (g/rho);
end

figure(1)
plot(t,v,'-')
xlabel("time [s]")
ylabel("velocity [ft/s]")
title("Velocity vs. Time (Steven Ortega)")

%% Problem 11.2

rho = 1.7;
g = 32.2;
p = 1;

h1 = 0.5;
t1 = 0: h1 :5;
v1(1) = 0;

for i = 1 : length(t1) - 1
    v1(i+1) = v1(i) + f_1(v1(i),rho,p,g)*h1;
end

h2 = 0.2;
t2 = 0: h2 :5;
v2(1) = 0;
for i = 1 : length(t2) - 1
    v2(i+1) = v2(i) + f_1(v2(i),rho,p,g)*h2;
end

figure(2)
plot(t,v,'-', t1,v1, 'ro', t2, v2, 'go')
xlabel("time [s]")
ylabel("velocity [ft/s]")
legend('Analytical Result', 'h = 0.5', 'h = 0.2')
title("Velocity vs. Time (Steven Ortega)")


%% Problem 11.3
rho = 1.7;
g = 32.2;
p = 1.1;

h3 = 0.1;
t3 = 0: h3 :5;
v3(1) = 0;

for i = 1 : length(t3) - 1
    v3(i+1) = v3(i) + f_1(v3(i),rho,p,g)*h3;
end

h3a = 0.05;
t3a = 0: h3a :5;
v3a(1) = 0;

for i = 1 : length(t3a) - 1
    v3a(i+1) = v3a(i) + f_1(v3a(i),rho,p,g)*h3a;
end

h3b = 0.025;
t3b = 0: h3b :5;
v3b(1) = 0;

for i = 1 : length(t3b) - 1
    v3b(i+1) = v3b(i) + f_1(v3b(i),rho,p,g)*h3b;
end

figure(3)
plot(t3,v3, 'b.', t3a,v3a, 'r.', t3b, v3b, 'g.', t,v)
legend('h = 0.1', 'h = 0.05', 'h = 0.025')
xlabel("time [s]")
ylabel("velocity [ft/s]")
title("Velocity vs. Time (Steven Ortega)")

%% Problem 11.4
rho = 1.7;
g = 32.2;
p = 1;

h4 = 0.5;
t4 = 0: h4 :5;
v4(1) = 0;

for i = 1 : length(t4)-1
    v_prime = f_1(v4(i),rho,p,g);
    vp = v4(i) + v_prime*h4;
    vn_prime = f_1(vp,rho,p,g);
    v4(i+1) = v4(i) + (h4/2)*(vn_prime+v_prime);
end

h5 = 0.2;
t5 = 0: h5 :5;
v5(1) = 0;

for i = 1 : length(t5)-1
    v_prime = f_1(v5(i),rho,p,g);
    vp = v5(i) + v_prime*h5;
    vn_prime = f_1(vp,rho,p,g);
    v5(i+1) = v5(i) + (h5/2)*(vn_prime+v_prime);
end

figure(4)
plot(t4,v4, 'bo', t5, v5, 'mo', t,v)
legend('Huen h = 0.5', 'Huen h = 0.2', 'Analytical')
xlabel("time [s]")
ylabel("velocity [ft/s]")
title("Velocity vs. Time (Steven Ortega)")

% Calculate RMS
for i = 1:length(t2)
    for j = 1: length(t)
        if t2(i) == t(j)
            time_to_comp_e(i) = i;
        end
    end
end

for i = 1:length(t5)
    for j = 1: length(t)
        if t5(i) == t(j)
            time_to_comp_h(i) = i;
        end
    end
end

for i = 1:length(t1)
    for j = 1: length(t)
        if t1(i) == t(j)
            time_to_comp_e1(i) = i;
        end
    end
end

for i = 1:length(t4)
    for j = 1: length(t)
        if t4(i) == t(j)
            time_to_comp_h1(i) = i;
        end
    end
end

sum_euler = 0;
for i = 1:length(time_to_comp_e)
    sum_euler = sum_euler + (v2(time_to_comp_e(i))-v(time_to_comp_e(i)))^2;
end
rms_euler = sqrt(sum_euler/length(time_to_comp_e));

sum_euler1 = 0;
for i = 1:length(time_to_comp_e1)
    sum_euler1 = sum_euler1 + (v1(time_to_comp_e1(i))-v(time_to_comp_e1(i)))^2;
end
rms_euler1 = sqrt(sum_euler1/length(time_to_comp_e1));

sum_heun = 0;
for i = 1:length(time_to_comp_h)
    sum_heun = sum_heun + (v5(time_to_comp_h(i))-v(time_to_comp_h(i)))^2;
end
rms_heun = sqrt(sum_heun/length(time_to_comp_h));

sum_heun1 = 0;
for i = 1:length(time_to_comp_h1)
    sum_heun1 = sum_heun1 + (v4(time_to_comp_h1(i))-v(time_to_comp_h1(i)))^2;
end
rms_heun1 = sqrt(sum_heun1/length(time_to_comp_h1));

fprintf("RMSE of Euler (h = 0.2) is: %.3f  |  RMSE of Euler (h = 0.5) is: %.3f\n" + ...
        "RMSE of Heun  (h = 0.2) is: %.3f  |  RMSE of Heun  (h = 0.5) is: %.3f\n", ...
        rms_euler, rms_euler1, rms_heun, rms_heun1)
%% Problem 11.5

rho = 1.5;
g = 32.2;
p = 1;

for h = [.1, .025, 0.01]
    t = 0:h:3;
    v_euler(1) = 0;
    v_heun(1) = 0;

    for i = 1:length(t)-1
        v_euler(i+1) = v_euler(i) + f_1(v_euler(i),rho,p,g)*h;

        v_prime = f_1(v_heun(i),rho,p,g);
        vp = v_heun(i) + v_prime*h;
        vn_prime = f_1(vp,rho,p,g);
        v_heun(i+1) = v_heun(i) + (h/2)*(vn_prime+v_prime);
    end

    figure(5)
    hold on
    plot(t,v_euler, '.')
    figure(6)
    hold on
    plot(t,v_heun, '.')
end
figure(5)
legend('h = 0.1', 'h = 0.025', 'h = 0.01')
title("Euler's Method")
xlabel("time [s]")
ylabel("velocity [ft/s]")
hold off

figure(6)
legend('h = 0.1', 'h = 0.025', 'h = 0.01')
title("Heun's Method")
xlabel("time [s]")
ylabel("velocity [ft/s]")
hold off