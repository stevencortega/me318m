%%% Lab 6 Assignment
% Date: 3/28/2022
% Name: Steven Ortega
% Section: 18265
clear all; clc;

%% Problem 7.1

mass = 2200;                    % [Kg]
g = 9.81;                       % [m/s2]
ang = [30, 35, 45, 65];         % [Degrees]

m = [ ...
     0, -cos(ang(1)+ang(2)), -cos(ang(3)), cos(ang(4)), 0; ...
     0, -sin(ang(1)+ang(2)), -sin(ang(3)), sin(ang(4)), 0; ...
     -cos(ang(1)), 0, 0, -cos(ang(4)), 0; ...
     -sin(ang(1)), 0, 0, -sin(ang(4)), mass*g; ...
     ];

n = length(m(:,1));             % get the number of eq's
x = [];                         % Solution vector
fprintf('----- Original:\n')
disp(m)
fprintf('----- RREF:\n')

% Pivoting to setup matrix m
for i = 1:n
    if m(i,i) == 0              % check if there is a 0 in the diagonal
        for j = i:n
            if m(j,i) ~= 0      % check other rows for non-zero and swap
                tmp = m(i,:);
                m(i,:) = m(j,:);
                m(j,:) = tmp;
                break;
            end
        end
    end
end

% Create upper diagonal
for i = 1:n
    for j = i+1:n
        if m(j,i) ~= 0
            m(j,:) = m(j,:) + (m(i,:) * (-m(j,i)/m(i,i)));
        end
    end
end

% Back Substitution
for i = n:-1:1
    x(i) = m(i, n+1) / m(i,i);              % save value
    m(i, :) = m(i, :) / m(i,i);             % make leading one's
    for j = i-1:-1:1                        % create RREF
        m(j,:) = m(j, :) - m(i, :)*m(j,i);
    end
end

disp(m)
fprintf('----- Tension Values:\n')
disp(x)

fprintf('Problem 7.1 RAN\n\n')

%% Problem 7.2

m = [ ...
     0, -cos(ang(1)+ang(2)), -cos(ang(3)), cos(ang(4)), 0; ...
     0, -sin(ang(1)+ang(2)), -sin(ang(3)), sin(ang(4)), 0; ...
     -cos(ang(1)), 0, 0, -cos(ang(4)), 0; ...
     -sin(ang(1)), 0, 0, -sin(ang(4)), mass*g; ...
     ];

a = m(:,1:4);
b = m(:,5);
c1 = inv(a)*b;
c2 = a \ b;

fprintf('----- Inverse Solution:\n')
disp(c1)

fprintf('----- Backslash Solution:\n')
disp(c2)

fprintf('Problem 7.2 RAN\n\n')

%% Problem 7.3

a = [1.001 1; 1 1];
b = [2; 1];

d = det(a);
fprintf('Determinant of A: %f\n',d)

c = a \ b;

disp(c)
fprintf('Problem 7.3 RAN\n\n')