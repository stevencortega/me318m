% Function 5.2

function [output] = g(x)
    output = ((2.*x) .* exp(cos(3.*x)) .* exp(-x)) + 70; 
end