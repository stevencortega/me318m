% Second Method Algorithm
%
% input:
%   x0 = lower bound
%   x1 = upper bound
% output:
%   root = root of f(x) if found

function root = secant(x0, x1)
    count = 0;  % begin tracking iterations
    while 1
        % compute the new root
        x = x1 - g(x1)*((x1 - x0) / (g(x1) - g(x0)));
        % exit conditions
        if (abs(g(x)) < 1e-3)
            root = x;
            return
        elseif count > 100
            disp('Root not found within 100 iterations')
            return
        end
        x0 = x1; x1 = x;
        count = count + 1;
    end
end