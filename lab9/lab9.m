%%% Lab 9 Assignment
% Date: 4/19/2022
% Name: Steven Ortega
% Section: 18265
clear all; clc;

%% Problem 10.1

fprintf('Acceleration at 24 seconds is: %.3f\n', (5/(24^.5)))
fprintf('The equation is 5/(t^.5)\n')

fprintf('Problem 10.1 Ran \n\n')

%% Problem 10.2

% Load the data
data = load("cardata.mat");
v = data.v;
t = data.t;

n = length(t);

% forward difference to get first value
aFirst = (v(2) - v(1)) / (t(2)-t(1));

% central difference to get middle values
for i = 2 : n-1
    a(i) = (v(i+1) - v(i-1)) / (2* (t(i+1)-t(i)));
end

% backward difference to get last value
aLast = (v(n) - v(n-1)) / (t(n)-t(n-1));

% combine data
a(1) = aFirst;
a(n) = aLast;

% plot data

figure(1)
plot(t,a,'o-')
    title('Time vs. Acceleration')
    xlabel('Time')
    ylabel('Acceleration')
    legend('Combined Methods')

fprintf('Acceleration at 24 seconds is: %.3f\n',a(7))

fprintf('Problem 10.2 Ran \n\n')

%% Problem 10.3

n = length(t);
pos(1) = 0; % given since t = 0  and v = 0
for i = 2: n
    pos(i) = trapIntegration(v,t(1:i));
end

figure(2)
plot(t,pos,'o-')
    title('Time vs. Position')
    xlabel('Time')
    ylabel('Position')
    legend('Trapezoidal Integration')

fprintf('The position at t = 24 is: %.3f\n', pos(7))

fprintf('Problem 10.3 Ran \n\n')

%% Problem 10.4

% Regular Simpsons is only one interval.
step = t(4)-t(1);

s = (step/3)*(v(1) + 4*v(4) + v(7));

fprintf('The position at t = 24 is: %.3f\n', s)

fprintf('Problem 10.4 Ran \n\n')

%% Problem 10.5

bma = t(7)-t(1);    % bounds

sum4 = 0;	% Summation with weight of 4
sum2 = 0;	% Summation with weight of 2

n = 7;
m = n-1;

% Compute the sum4 terms
for i = 2 : 2 : n-1
	sum4 = sum4 + v(i); 
end

% Compute the sum2 terms
for i = 3 : 2 : n-2
	sum2 = sum2 + v(i);
end

% Simpsons rule
S = ((bma) * (v(1) + 4*sum4 + 2*sum2 + v(7)))/(3*m);

fprintf('The position at t = 24 is: %.3f\n', S)


% Compute Relative error wrt Analytical Sol: 20/3 * t^1.5 = 783.8367

relTrap = abs(pos(7) - 783.8367)/ 783.8367;
relSimp = abs(s - 783.8367)/783.8367;
relCompSimp = abs(S - 783.8367)/783.8367;

fprintf("\nMethod         |   Rel. Error \n" + ...
         "-------------------------------\n" + ...
         "Trapezoidal    | %.3f\n" + ...
         "Simpsons       | %.3f\n" + ...
         "Comp. Simpsons | %.3f\n\n",relTrap, relSimp, relCompSimp)
% Based on these results we can see that the composite simpsons is more
% accurate than regular simpsons (smaller intervals). The trapezoidal is
% the most accurate, probably due to the position equation form.

fprintf('Problem 10.5 Ran \n\n')