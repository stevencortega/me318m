%%% Lab 4
% Date: 2/28/2022
% Name: Steven Ortega
% Section: 18265
clear; clc;

%% Problem 4.1

% Run twice for TA grading (10-23, inclusive and exclusive)
for TA = 1:1:2
    % get all the inputs
    lowerLimit = input('Lowerlimit? ');
    upperLimit = input('Upperlimit? ');
    choice = input('Inclusive? (yes = 1/no = 0): ');
    
    % create the range
    myRange = [];
    if choice
        for n = lowerLimit : 1 : upperLimit
            myRange = [myRange n];
        end
    else
        for n = (lowerLimit+1) : 1 : (upperLimit-1)
            myRange = [myRange n];
        end
    end
    
    % find the length, sum, and average
    lenMyRange = length(myRange);
    mySum = sum(myRange);
    myAverage = mySum/lenMyRange;
    
    % display results
    fprintf('\n\nThe values are:\n length: %i\n sum: %i\n avg: %.3f\n', ...
        lenMyRange, mySum, myAverage);
end

disp('Problem 4.1 Ran')
%% Problem 4.2

% create a loop until a valid age is entered and confirmed
validInput = 0;
while ~validInput
    usrAge = input('How old are you? ');
    
    % check for validity. if invalid age continue to ask
    while ~(usrAge > 0) || ~(usrAge<120)
        disp('This is not a valid human age!')
        usrAge = input('How old are you? ');
    end
    
    % check with the user that the age entered is correct. If not restart
    % the while loop to reprompt the user
    usrAgeCheck = input("You just told me that you are " + usrAge + ...
        " years old. Is that correct? Yes or No ->", "s");
    % check for confirmation
    if strcmpi(usrAgeCheck, 'yes') || strcmpi(usrAgeCheck, 'y')
        disp("This confirms that you are " + usrAge + " years old!")
        validInput = 1;         % end the loop and exit
    else
        disp("OK, there was a mistake, so I'll ask you again.")
    end
end

disp('Problem 4.2 Ran')
%% Problem 4.3

% create two matricies and populate
matrixA = [1 2 3 4;
           5 6 7 8;
           9 1 2 3];
matrixB = [1 2 3 4;
           5 6 7 8;
           9 1 2 3];

% instantiate the final matrix
matrixC = [];
for col = 1:1:4
    for row = 1:1:3
        matrixC(row, col) = matrixA(row, col) + matrixB(row,col);
    end
end
disp(matrixC)

disp('Problem 4.3 Ran')
%% Problem 4.4

% get displacement values
time = 0 : 0.01 : 1.8;  % time values vector
displace = [];
for t = time
    displace = [displace, myfun(t)];
end

% find and store values < 0.01 with corresponding time
% output nx2 matrix with [t, displacement] format
absDisplacement = [];
for n = 1 : 1 : length(time)
    if (abs(displace(n)) < 0.01)
        absDisplacement = [absDisplacement; [time(n), displace(n)]];
    end
end

% plot the data
plot(time, displace, 'b')
    hold on

% plot the points with displacement < 0.01
plot(absDisplacement(:, 1), absDisplacement(:, 2), 'o')
    hold off
    % format
    title('Time vs Displacement')
    xlabel('Time (s)')
    ylabel('Displacement')
    grid on
    
% save plot
h1 = figure(1);
saveas(h1, 'prob_4-4.jpg');

% display time values for <0.01 displacement
disp("Time where displacement is < 0.01: " + absDisplacement(:,1))

disp('Problem 4.4 Ran')
%% Problem 4.5

% get number of times to prompt user
N = input("Input an integer >= 10: ");
% check validity of input
while ~(N >= 10)
    N = input("Input an integer >= 10: ");
end

% store N input from user
allInputs = [];
for n = 1:1:N
    usrInput = input("Enter a number of this form 1.23e4 or 1.23e-4: ");
    allInputs = [allInputs usrInput];
end

% find product of all inputs and display
productAll = prod(allInputs);
fprintf('The product of all the numbers you entered is %.3f\n', productAll)

disp('Problem 4.5 Ran')
%% Problem 4.6

% get number of times to prompt user
N = input("Input an integer >= 10: ");
% check validity of input
while ~(N >= 10)
    N = input("Input an integer >= 10: ");
end

% store N input from user
allInts = [];
for n = 1:1:N
    usrInt = input("Enter an integer: ");
    % checks if the input is an integer and rounds if not
    if mod(usrInt, 1) ~= 0
        usrInt = ceil(usrInt);
        disp("The number you entered is not an integer, so I'm " + ...
            "rounding it up to " + usrInt);
    end
    allInts = [allInts usrInt];
end

disp('Integers chosen:')
disp(allInts);

% sort the integers into even positive/negative and odd arrays
posInts = []; negInts = []; oddInts = [];
for n = allInts
    if (mod(n,2) == 0) && (n >= 0)
        posInts = [posInts n];
    elseif (mod(n,2) == 0)
        negInts = [negInts n];
    else
        oddInts = [oddInts n];
    end
end

disp(posInts)
disp(negInts)
disp(oddInts)
disp("The original array of " + N + " integers contains " + ...
    length(posInts) + " positive even integers, " + ...
    length(negInts) + " negative even integers, and " + ...
    length(oddInts) + " number of odd integers.")

disp('Problem 4.6 Ran')
%% Problem 4.7

% %--------------------------------------------
% % DRY RAN CODE ON PAPER
% ERROR 1 n accepts a string, should be an integer
% n = input(’How many numbers in the sequence do you want to see?’,’s’);
% mytrian = zeros(1,n); %Vector to store the sequence in

% ERROR 2 the bounds on the for loop are wrong shoud be 1:n
% for i = 1:1:n-2

% ERROR 3, 4, and 5.
    % error 3: typo mytran -> mytrian
    % error 4: Missing () on n+1 term
    % error 5: switch n to i
%   mytran(i) = (n/2)*n+1; %Triangular sequence rule
% end
% disp (’The Triangular sequence is ’);

% error 6: vec doesnt exist
% vec %Show the contents of the output vector
% 
% %-----------------------------------------

n = input('How many numbers in the sequence do you want to see?'); % extra 's'
mytrian = zeros(1,n); %Vector to store the sequence in
for i = 1:1:n % extra -2 on the n-2 term
    mytrian(i) = (i/2)*(i+1); %Triangular sequence rule   --TYPO HERE + missing () + switch n to i
end
disp ('The Triangular sequence is ');
disp(mytrian) %Show the contents of the output vector

disp('Problem 4.7 Ran')
%% Problem 4.8

usrPal = input ('Enter a word to see if it is a palindrome: ','s');
is_palindrome(usrPal)

% test the function is_palindrome for TA
is_palindrome('hannah')
is_palindrome('deleveled')
is_palindrome('racecar')
is_palindrome('selfless')
is_palindrome('detartrated')

disp('Problem 4.8 Ran')
%% Problem 4.9

usrRev = input('Enter a phrase to reverse: ', 's');
disp(RevertString(usrRev))

% Test for TA
test1 = RevertString('Michael D. Bryant');
test2 = RevertString('Ashish D. Despande');
test3 = RevertString('Steven Ortega');
fprintf('Test1: %s \nTest2: %s\nTest3: %s\n',test1, test2, test3)

disp('Problem 4.9 Ran')
%% Problem 4.10
usrTri = input('What row of the triangle would you like? ');
disp("The row for " + usrTri + " is:")
disp(solvePascal(usrTri))

% test it for TA
n = 8;
result = solvePascal(n);
disp("The row for " + n + " is:")
disp(result)

disp('Problem 4.10 Ran')
