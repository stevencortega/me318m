%%% Lab 8 Assignment
% Date: 4/8/2022
% Name: Steven Ortega
% Section: 18265
clear all; clc;

%% Problem 9.1

x = [.15, 1.04, 1.44, 1.84, 2.24, 2.64, 3.04, 3.44, 3.84, 4.24];
y = [7.5, 5.6, 4.4, 3.6, 3.0, 2.5, 2.2, 1.9, 1.5, 1.1];

f = -2.*x + 8;

% plot the data points
plot(x,f, 'r')
	title('Amplitude vs Distance')
	xlabel('Distance')
	ylabel('Amplitude')
	grid on
	hold on
plot(x,y, 'bo')
    legend('Data Points', 'Best Fit')
	hold off

fprintf('Problem 9.1 Ran\n\n')

%% Problem 9.2

e = f - y;
e = e.^2;
e = sum(e);

fprintf('Value for e is: %.3f\n',e)

fprintf('Problem 9.2 Ran\n\n') 

%% Problem 9.3

% Get best fit line
coeff = polyfit(x,y,1);
best_fit = polyval(coeff, x);

figure(2) 
plot(x,y, 'ro', x, best_fit, 'g')
	title('Amplitude vs Distance')
	xlabel('Distance')
	ylabel('Amplitude')
	grid on
    legend('Data Points', 'Best Fit')

% Calculate error
e = best_fit - y;
e = e.^2;
e = sum(e);

fprintf('Value for e is: %.3f\n',e)

fprintf('Problem 9.3 Ran\n\n') 

%% Problem 9.4

% part a
yprime = log(y);

figure(3)
plot(x, yprime,'o')
    title('Distance vs ln(Amplitude)')
    xlabel('Distance')
    ylabel('ln(Amplitude)')
    grid on

% part b

% calculate the best fit line
coeff = polyfit(x, yprime, 1);
best_fit = polyval(coeff, x);

% plot the best fit line
hold on
plot(x, best_fit)
legend('ln(y)','best fit')
hold off

fprintf('Coefficients are: ')
disp(coeff)

% part c
b = -coeff(1);
a = exp(coeff(2));

fprintf('A is %.3f and B is %.3f \n', a, b)

f = a.*exp(-b.*x);
% part d
figure(4)
plot(x, y,'ro', x, f, 'g')
    title('Distance vs ln(Amplitude)')
    xlabel('Distance')
    ylabel('ln(Amplitude)')
    grid on
    legend('Data Points', 'Best Fit')

fprintf('Problem 9.4 Ran\n\n') 

%% Problem 9.5

% The sound amplitude at x = 4 can be given by plugging in 4 into the best
% fit equation line.

f4 = a*exp(-b*4);

fprintf('The predicted value at x = 4 is: %.3f \n', f4)

fprintf('Problem 9.5 Ran\n\n')

%% Problem 9.6

% Possible Issues:
%
% 1) If we don't know what curve fitting model to use, the model chosen may
% not be the best one. For example the straight line and polynomial models
% did not fit as well as the power fit.
%
% 2) If the data has a lot of variablility and is not accurate (large
% margins of error) any curve fitting will need a lot of data points to
% account for the error. Otherwise the error will change the best fit line
% by a lot.
%
% 3) If there is a lot of data or data with an odd shape using a polynomial
% method would probably work best but the amount of terms used to
% accurately represent the data would create a large matrix to solve and be
% very expensive computationaly.

fprintf('Problem 9.5 Ran\n\n')