% Trapezoidal Numerical Integration

function s = trapIntegration(v,t)
    n = length(t);
    s = 0;
    for i = 1 : n-1
        s = s + (v(i) + v(i+1));
    end
    s = s * (t(n)-t(n-1))/2;
end