% Problem 8.2 Jacobean Function
% Lab 7

function [result] = jacob_8_2(angles)
    % define the constants/knowns
    d1 = 1;             % [m]
    d2 = 1;             % [m]

    % separate alpha and beta
    alpha = angles(1);
    beta = angles(2);

    % calculate the jacobean
    result = [-1.* (d1 .* sin(alpha)) + (d2 .* sin(alpha + beta)), ...
              -d2 .* sin(alpha + beta); ...
              (d1 .* cos(alpha)) + (d2 .* cos(alpha + beta)),...
              d2 .* cos(alpha + beta)];
end