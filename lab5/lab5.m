%%% Lab 5 Assignment
% Date: 3/4/2022
% Name: Steven Ortega
% Section: 18265
clear all; clc;

%% Problem 5.1
A = [0, pi]; b = -pi;

% test the functions with A and b
disp("f(A): ")
disp(f(A))

disp("f(b): ")
disp(f(b))

disp("g(A): ")
disp(g(A))

disp("g(b): ")
disp(g(b))

fprintf('Problem 5.1 RAN\n\n')

%% Problem 5.2

% create the interval for f(x)
x = -10 : 0.1 : 20;
y = f(x);

figure(1)
plot(x, y, 'k')
    title('Graph of f(x)')
    xlabel('Values of x')
    ylabel('f(x)')
    grid on
   
disp('Values where f(x) = 0 are approximately at:')
fprintf('  -0.7, 2.3, 16.9\n\n')

fprintf('Problem 5.2 RAN\n\n')

%% Problem 5.3

% save the root of my inital guess
root = bisection(3, 50);
disp('The interval is: 3, 50')
fprintf('The root is: %.5f \n\n', root)

% create the interval for f(x)
x = -10 : 0.1 : 20;
y = f(x);

% ------------------- Instructions make duplicate
figure(2)
plot(x, y, 'k')
    title('Bisection of f(x)')
    xlabel('Values of x')
    ylabel('f(x)')
    grid on
% --------------------

% plot the roots
hold on 
plot(root, f(root), 'ro')

% make y = 0 axis more visible
plot(x, zeros(length(x)),'b')
hold off

fprintf('Problem 5.3 RAN\n\n')

%% Problem 5.4

% create the interval for g(x)
x = -3.5 : 0.05 : -1.5;
y = g(x);

% Plot g(x)
figure(3)
plot(x, y, 'k')
    title('False Position g(x)')
    xlabel('Values of x')
    ylabel('g(x)')
    grid on
hold on

% Plot the roots of the function
roots = [false_position(-3.5, -3), ...  % First initial guess
         false_position(-3,-2.5), ...   % Second initial guess
         false_position(-2.5,-1.5)];    % Third initial guess
plot(roots, g(roots),'ro')

% Display chosen intervals
fprintf('The intervals are:\n (-3.5, -3)\n (-3,-2.5)\n (-2.5,-1.5)\n\n')

% make the roots more obvious
plot(x, zeros(length(x)),'b')
hold off

fprintf('Problem 5.4 RAN\n\n')

%% Problem 5.5

root1x = bisectiong(4,6);
disp ("Root: "+ root1x + ", g(root):" + g(root1x))
fprintf('\n')
% I got about 70, which is not close to 0

x = -1 : 0.1 : 6;
figure(4)
plot(x, g(x))

% The interval from 4-6 never crosses the y-axis and therefore has no roots
% there. This explains why the bisection method gave me the wrong answer.

fprintf('Problem 5.5 RAN\n\n')

%% Problem 6.1

x = -10 : 0.1 : 20;
y = f(x);

figure(5)
plot(x, y, 'k')
    title("Newton's f(x)")
    xlabel('Values of x')
    ylabel('f(x)')
    grid on

% Plot the roots of the function
roots = [newt(-9), ...  % First initial guess
         newt(5), ...   % Second initial guess
         newt(19)];    % Third initial guess
hold on
plot(roots, f(roots),'ro')

% display the guesses
fprintf('The initial guesses are:\n -9\n 5\n 19\n\n')

% make the roots more obvious
plot(x, zeros(length(x)),'b')
hold off

fprintf('Problem 6.1 RAN\n\n')

%% Problem 6.2

x = -3.5 : 0.05 : -1.5;
y = g(x);

figure(6)
plot(x, y, 'k')
    title("Secant g(x)")
    xlabel('Values of x')
    ylabel('f(x)')
    grid on

% Plot the roots of the function
roots = [secant(-3.5,-3), ...  % First initial guess
         secant(-3,-2.5), ...   % Second initial guess
         secant(-2.5,-1.5)];    % Third initial guess
hold on
plot(roots, g(roots),'ro')

% Display chosen intervals
fprintf('The intervals are:\n (-3.5, -3)\n (-3,-2.5)\n (-2.5,-1.5)\n\n')

% make the roots more obvious
plot(x, zeros(length(x)),'b')
hold off

fprintf('Problem 6.2 RAN\n\n')

%% Problem 6.3

x = -10 : 0.1 : 20;
y = f(x);

figure(7)
plot(x, y, 'k')
    title("Newton's f(x)")
    xlabel('Values of x')
    ylabel('f(x)')
    grid on

% Plot the roots of the function
roots = [newt(11.61), ...
         newt(11.62)];
hold on
plot(roots, f(roots),'ro')

% make the roots more obvious
plot(x, zeros(length(x)),'b')
hold off

%---------Zoomed in plot---------
x = 11.605 : 0.0001 : 11.625;
y = f(x);

figure(8)
plot(x, y, 'k')
    title("Newton's f(x)")
    xlabel('Values of x')
    ylabel('f(x)')
    grid on

% Plot the points of the function
points = [11.61, 11.62];
hold on
plot(points, f(points),'ro')
hold off

% The f'(x) values for 11.61 and 11.62 have different sign values.
% Therefore when the algorithm begins they search for points away from the
% 0 slope. Since the new point is chosen based off of f(x)/f'(x) a
% different sign slope means a different direction for the next guess of x.

fprintf('Problem 6.3 RAN\n\n')