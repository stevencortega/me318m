% Order Array Testing
%
% Test order_array() in a variety of situations

% create the tests
test1 = 'longhorns';
test2 = [0.5 -9.81 pi 10 sqrt(2)];
test3 = 'sco536';

% unit tests requested
assert(isequal(sort(test1, 'descend'), order_array(test1, 'D')))
assert(isequal(sort(test2), order_array(test2, 'A')))
assert(isequal(sort(test3), order_array(test3, 'A')))
    
% additional unit tests
assert(isequal(sort(test1), order_array(test1, 'A')))
assert(isequal(sort(test2, 'descend'), order_array(test2, 'D')))
assert(isequal(sort(test3, 'descend'), order_array(test3, 'D')))

% return true if everything passed
disp('All tests passed')
