function answer = dydtF(t, y)
    R = 0.1;
    C = 1.2;
    answer = ((24.9*exp(-t/0.07)*sin(2*pi*t/0.035))-y)/(R*C);
end