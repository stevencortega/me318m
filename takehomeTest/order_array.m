% Order Array Algorithm
%
% Using the selection sort algorithm (not very efficient but easy to code)
%
% Find the min/max element and swap with first element in the array. Then
% move onto the next element and find the min/max and swap. Continue until
% we iterate through the whole array.

% input:
%   a = array
%       - Numbers or text
%   c = char naming direction
%       - 'A' or 'D'
% output:
%   a = same array 'a' sorted in direction specified by c

function a = order_array(a, c)
    % Sort in descending order
    if c == 'D'
        for i = 1:length(a)             % iterate through the array
            maxVal = a(i);              % set current max
            maxIdx = i;                 % store index
            % iterate through the remaining elements
            for j = i+1:length(a)       
                % if new current max found, store it
                if a(j)>maxVal
                    maxVal = a(j);
                    maxIdx = j;
                end
            end
            % swap the max to our position in the array
            a(maxIdx) = a(i);
            a(i) = maxVal;
        end
    % Sort in ascending order
    elseif c == 'A'
        for i = 1:length(a)             % iterate through array
            minVal = a(i);              % set current min val
            minIdx = i;                 % store index
            % iterate through remaining elements
            for j = i+1:length(a)
                % if new min is found, store it
                if a(j)<minVal
                    minVal = a(j);
                    minIdx = j;
                end
            end
            % swap the min to our position in the array
            a(minIdx) = a(i);
            a(i) = minVal;
        end
    end
end