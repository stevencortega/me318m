% Function f'(x)

function [output] = fprime(x)
    output = (3/500) .* ((3.* x.^2) - (37.07 .* x) + 25.697);
end