% False Position Method Algorithm
%
% input:
%   xa = lower bound
%   xb = upper bound
% output:
%   root = root of g(x) if found

function root = false_position(xa, xb)
    % argument validity
    if xa == xb
        disp('invalid interval')
        return
    end

    count = 0;  % begin tracking iterations
    while 1
        c = xb - ((g(xb) * (xb-xa)) / (g(xb)-g(xa)));
        yc = g(c);
        % exit conditions
        if (abs(yc) < 1e-3)
            root = c;
            return
        elseif count > 100
            disp('Root not found within 100 iterations')
            return
        else
            % find the new midpoint and intervals
            if (g(xa) * yc) < 0
                xb = c;
            else
                xa = c;
            end
            count = count + 1;
        end
    end
end