% solvePascal
% solves pascals triangle and returns nth row of the triangle
%
% Input: int n
% Output: array with numbers in that row

function tri = solvePascal(n)
    
    % return known values
    if n == 1
        tri = 1;
        return
    elseif n == 2
        tri = [1 1];
        return
    end

    % First two rows are 1
    tri(1, 1) = 1;
    tri(2, 1 : 2) = [1 1]; 
    
    for row = 3 : n
        tri(row, 1) = 1;   % first number of row is 1
        
        % sum the two numbers above it
        for col = 2 : row-1
            tri(row, col) = tri(row-1, col-1) + tri(row-1, col);
        end   
    
        tri(row, row) = 1;      % last number of row is 1
    end
    
    tri = tri(n, :); % output the row requested
end