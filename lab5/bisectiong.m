% Bisection Method Algorithm
%
% input:
%   xa = lower bound
%   xb = upper bound
% output:
%   root = root of g(x) if found

function root = bisectiong(xa, xb)
    % argument validity
    if xa == xb
        disp('invalid interval')
        return
    end

    count = 0;  % begin tracking iterations
    while 1
        % exit conditions
        if (abs(xb-xa) < 1e-4)
            root = (xa + xb)/2;
            return
        elseif count > 20
            disp('Root not found within 20 iterations')
            return
        else
            % find the new midpoint and intervals
            c = (xa + xb)/2;
            if (g(xa) * g(c)) < 0
                xb = c;
            else
                xa = c;
            end
            count = count + 1;
        end
    end
end