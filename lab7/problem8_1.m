% Lab 7
% Problem 8.1


% define the constants/knowns
d1 = 1;             % [m]
d2 = 1;             % [m]
alpha = 0 : pi/100 : pi/2;      % [radians]
beta = 0 : pi/100 : pi;         % [radians]

% create the plot intervals and calculate range
[x, y] = meshgrid(alpha, beta);

r = (d1 .* cos(x)) + (d2 .* cos(x + y));
h = (d1 .* sin(x)) + (d2 .* sin(x + y));

figure(1)
% Plot the range
subplot(2,1,1), mesh(x,y,r)
    title('Range of robot arm')    
    xlabel('Alpha [rads]')
    ylabel('Beta [rads]')
    zlabel('Range [m]')
    
% Plot the height
subplot(2,1,2), mesh(x,y,h)
    title('Height of robot arm')
    xlabel('Alpha [rads]')
    ylabel('Beta [rads]')
    zlabel('Height [m]')

% Discussion
% 1) The angles with the greatest heights are when beta is 0 and alpha is
%       at maximum (aka pi/2, straight up)
% 2) The Height can never go negative because alpha is always > 0 and so is
%       beta. Since neither angle is ever negative the arm never goes below
%       base height. This makes sense because alpha is only 0-90 degrees
%       and beta can only bend until it is back at the origin.