% RevertString
% reverses the string
%
% Input: string s
% Output: reversed string revStr

function [revStr] = RevertString(s)
    %revStr = reverse(s);
    
    % ------ Long way -------
    revStr = '';
    for i = 0:1:(length(s)-1)
        currentLetter = s(length(s)-i);
        revStr = append(revStr, currentLetter);
    end
end